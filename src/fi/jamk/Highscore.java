/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fi.jamk;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author H4273
 */
public class Highscore implements Serializable{
    private ArrayList<Pelaaja> highscorelista;
    private final String tiedostonNimi = "highscore.data";
    
    // konstruktori
    public Highscore() {
        this.highscorelista = new ArrayList<>();
        //alustaLista();
    }
    
    // tallennetaan highscorelista tiedostoon
    public void tallenna() {
        // Kalojen Tallennus tiedostoon
        ObjectOutputStream output = null;
        try {
            output = new ObjectOutputStream(new FileOutputStream(new File(tiedostonNimi)));
            output.writeObject(highscorelista);
         } catch (IOException ex) {
            System.out.println("Virhe tiedostoon kirjoittamisessa :" + ex);
        } finally {
            try {
                if (output != null) output.close();
            } catch (IOException ex) {
                System.out.println("Virhe tiedoston sulkemissa : " + ex);
            }
        }
    }
     // Highscoren Lukeminen tiedostosta
    public void lue() {
        ObjectInputStream input = null;
        try {
            input = new ObjectInputStream(new FileInputStream(new File(tiedostonNimi)));
            highscorelista = (ArrayList<Pelaaja>) input.readObject();
        } catch (IOException ex) {
            System.out.println("Virhe tiedoston lukemisessa :" + ex);
        } catch (ClassNotFoundException e) {
        } finally {
            try {
               if (input != null) input.close();
            } catch (IOException ex) {
                System.out.println("Virhe tiedoston sulkemisessa : " + ex);
            }
        }
        // debuggausta konsolille
        System.out.println("Highscore lista luettu uudelleen tiedostosta.");
        //tulosta();
    }
    // luetaan tiedostosta, tai luodaan oletuskalat
    public void alustaLista() {
        File f = new File(tiedostonNimi);
        if (!f.exists()) {
            highscorelista.add(new Pelaaja("Hauki", 1));
            highscorelista.add(new Pelaaja("Siili", 2));
            highscorelista.add(new Pelaaja("Kotka", 3));
            highscorelista.add(new Pelaaja("Hauki", 4));
            highscorelista.add(new Pelaaja("Siili", 5));
            highscorelista.add(new Pelaaja("Kotka", 6));
            highscorelista.add(new Pelaaja("Hauki", 7));
            highscorelista.add(new Pelaaja("Siili", 8));
            highscorelista.add(new Pelaaja("Kotka", 9));
            highscorelista.add(new Pelaaja("Hauki", 10));
            tallenna();
        } else {
            lue();
        }
        tulosta();
    }
    
    // tulostetaan pelaajat output-ikkunaan
    public void tulosta() {
        for (Pelaaja k : highscorelista) {
            System.out.println(k);
        }
    }
    
    // palauttaa highscorelistan
    public ArrayList<Pelaaja> palauta() {
        return highscorelista;
    }
    
    // lisää uuden pelaajan
    public void lisaaUusi(String nimi,int pisteet) { 
        highscorelista.add(new Pelaaja(nimi,pisteet));        
        tallenna();            
    }
    
    // Pelaajien järjestäminen (sort) uudelleen
    public void jarjesta() {
        Collections.sort(highscorelista, new SortPelaajas());
        System.out.println("Highscorelista on järjestetty pisteiden mukaan.");
        tallenna();            
    }
    
    // järjestää kalat painon mukaan järjestykseen
    class SortPelaajas implements Comparator<Pelaaja> {    
        @Override
        public int compare(Pelaaja p1, Pelaaja p2) {
            if (p1.getScore() > p2.getScore()) return -1;
            if (p2.getScore() < p1.getScore()) return 1;
            return 0;
        }
    }
}//class
    


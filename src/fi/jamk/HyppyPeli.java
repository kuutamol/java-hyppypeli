/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.jamk;
// importataan seuraavat paketit
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javazoom.jl.player.Player;

/**
 *
 * @author lasse
 */
public class HyppyPeli extends JFrame implements Runnable{    // uusi luokka joka hyödyntää JFrame:a 
    private Ball ball; 
    
    private Thread thread;
    private final int WIDHT = 500;                            // ikkunan leveys
    private final int HEIGHT = 600;                           // ikkunan korkeus
    private final int BLOCKS = 200;                           // blockien/seinien lukumäärä kokonaisuudessaan
    private ArrayList<Block> blocks;                          // luodaan arraylist, sisältävät blokkeja
    private boolean collides = false;                         // törmääkö vai eikö törmää
    private int mx=0, my=0;                                   // ? selvitetään
    private int gameMode;                                     // pelimoodi/näkymä määrittely
    private Highscore highscore;
    JButton pelaabtn = new JButton("Uusi peli");                  // napit...
    JButton highscorebtn = new JButton("High Score"); 
    JButton lopetabtn = new JButton("Lopeta");
    JButton takaisinbtn = new JButton("Takaisin");
    JButton tallennabtn = new JButton("Tallenna ja palaa");
    JTextField nimiField = new JTextField("Anon");
    private int trip = 0;                                         // ei vielä käytössä, tulossa pisteiden laskuun
    private int score = 0; 
    private Image menu;
    private Image highscore_img;
    private Soitin soitin;
    private Image pelibg;
    private Image gameover_img;
    private int liikuta;
    
    Player player = null;
    //Image img = ImageIO.read(new File("hyppypeli.jpg"));
    
    
    
    public HyppyPeli(){                                       // käynnistää pelin varsinaisen näkymän, hyppypelin konstruktori
        super("HyppyPeli");
        setDefaultCloseOperation(EXIT_ON_CLOSE);              // sulkemisen toiminta
        setSize(WIDHT,HEIGHT);                                // mittojen muuttujat
        setResizable(false);                                  // ikkanan kokoa ei voida muuttaa arvolla false
        soitin = new Soitin();
        
        highscore = new Highscore();
        gameMode = 1; 
        // menu valikko oletuksena
        menu = getToolkit().getImage("päävalikko.jpg");
        highscore_img = getToolkit().getImage("highscore.jpg");
        pelibg = getToolkit().getImage("pelitausta.jpg");
        gameover_img = getToolkit().getImage("gameover.jpg");
        highscore.alustaLista();
        //highscore.jarjesta();
        highscore.lue();
        
        getContentPane().add( new Piirtopaneeli() ); 
        getContentPane().setBackground(Color.WHITE);
        
        
        // luodaaan piirtopaneeli
        
        addMouseListener(new HiirenPainallukset());           // lisätään hiirentoimintojen kuuntelu

        pelaabtn.addActionListener(new ActionListener() {     // pelaabtn-napin toiminnon kuuntelija
         public void actionPerformed(ActionEvent e) {         // kun napin toiminto aktivoituu niin tehdään seuraavaa...
                luoUusiSäie();                                  //Luodaan uusi säie             
                ball = new Ball(WIDHT,HEIGHT);                  //Luodaaan uusi pallo     
                Random randomGenerator = new Random();          //uudet satunnaisgeneraattorit
                int rand[] = new int[200];                      
                for (int j = 0; j < 200; ++j){                   // alustetaan uusi apumuuttuja j jonka arvo aluksi 0, ehton että j:n arvo oltava alle 200, kasvatetaan jokaisella kierroksella j:n arvoa yhdellä
                  rand[j] = randomGenerator.nextInt(250);       // asetaan rand-nimiseen taulukkoon (aikaisemmin kerrotun mukaisesti) j-arvoiseen soluun, satunnaisuus 250 saakka
                }
                                                                //Luodaan uudet palikat
                blocks = new ArrayList<>();
                for(int i =1;i<=BLOCKS;i++){
                    
                    blocks.add(new Block(i,rand));
                }
                                                                //säie käytnnistyy ja peli käynnistuu gamemode 2 == peli käynnissä
                thread.start();
                gameMode=2;                                     // varsinainen pelinäkymään sirtyminen switch-case muututjan arvoa muuttamalla
                collides = false;                               // resetoidaan törmäyksen arvo tässätapauksessa falseksi
                score = 0;                                      // resetoidaan score
                trip = 0;                                       // resetoidaan trip
                liikuta = 0;
         }
        });
        
        lopetabtn.addActionListener(new ActionListener() {      // lopetabtn-napin toiminnon kuuntelija
         @Override
         public void actionPerformed(ActionEvent e) {
            System.exit(0);                                     // sovellus lopetetaan eli ikkuna sulkeutuu 
         }
        });
        highscorebtn.addActionListener(new ActionListener() {  // highscorebtn-napin toiminnon kuuntelija
         @Override
         public void actionPerformed(ActionEvent e) {
            System.out.println ("painoit pistetaulukkonappia"); // palautteeksi
            gameMode=3;                                         //highscore
            repaint();
            // thread = null;
         }
        });
        
        takaisinbtn.addActionListener(new ActionListener() {  // takaisinbtn-napin toiminnon kuuntelija
            @Override
            public void actionPerformed(ActionEvent e) {
               gameMode=1; //menu
               repaint(); 
            }
        });
        tallennabtn.addActionListener(new ActionListener() {  // takaisinbtn-napin toiminnon kuuntelija
            @Override
            public void actionPerformed(ActionEvent e) {
               String nimi = nimiField.getText();
               highscore.lisaaUusi(nimi, score);
               gameMode = 1;
               repaint(); 
            }
        });
    }
    
    //luo uusi säie metodi
    public void luoUusiSäie() {
            if (thread==null) thread = new Thread(this);        // jos säikeen arvo on null niin luodaan uusi säie 
    }

    //hiirenpainallukset käsitellään tässä luokassa
    class HiirenPainallukset extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e){
            //Jos peli on käynnissä pallon hyppy toiminnassa
            if (gameMode==2) {
            ball.mmove();
            //ball.color();
            System.out.println("Hyppy");                        // tulostetaan palautteeksi (jokaisella klikkauksella) sana "Hyppy"
            }
        }
    }
    public void liikuta(){
    
    
    
}
    
    //Törmäyksen tarkistaminen
    public void checkCollision() {
        // pallo osuu blockiin     
        for (Block block : blocks) {                            // Block = luokka, block = olio, blocks(vihreällä) = arraylisti; eli arraylist käydään läpi
            if (hitTest(ball.getX(),ball.getY(),ball.getSize(),ball.getSize(),   // tutkitaan if-lauseella täsmäävätkö koskaan pallon ja yhden blockin koordinaatit
                        block.getX(),block.getY(),block.getSizeX(),block.getSizeY())) {
                 collides = true;                               // muutetaan collides-booleanin arvoksi true eli tosi jos ylemmän ehto täyttyy
                 gameMode = 4;                                  // muutetaan case:n arvoksi 1 eli tämä siirtää näkymän menuun eli valikkoon
                 
            }
        }
    }
    public boolean hitTest(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2) {  // Seuraavaan purettu auki aikaisemmin if-lauseessa käytetty "hitTest"
        w2 += x2;
        w1 += x1;
        if (x2 > w1 || x1 > w2) return false;
        h2 += y2;
        h1 += y1;
        if (y2 > h1 || y1 > h2) return false;
	return true;                                                                          // palauttaa if-lauseen arvoksi true (eli tuolloin collides-booleanin arvoksikin tulee true)
    }

    //Tämä run metodi suoritetaan omassa säikeessä, ei mainissa
    @Override
    public void run(){
        
        while(true){
            liikuta-=2;
            // Pisteiden kerryttäminen
            if(ball.getX()> blocks.get(trip).getX()){
                score++;
                trip += 2;
            }
            
            if(ball.getY()> HEIGHT || ball.getY()< 0) {                                        // jos pallon korkeus on suurempi kuin ikkunan alareuna (eli pallo putoaa maahan) TAI pallo menee yli näytön yläreunan niin ...
                break;                                                                         // ...katkaisee/lopettaa säikeen 
            }
            //game engine
            ball.move();                                    //siirrä palloa
            //ball.color();           //siirrä palloa
            //ball.color();
            for(Block block : blocks)block.move();          //siirrä palikoita
            checkCollision();                               //tarkasta törmäys
            //Score
            repaint();                                      //piirrä näyttö uudelleeen
            if(gameMode == 4)break;                         // jos game mode on mallia mainmenu lopeta säie
            try{            
            Thread.sleep(25);                               //huilaa hetki
            } catch (InterruptedException e){               // virheen tarkistus
                
            }
            
        }
        
        gameMode =4;//Siirry gameover valikkoon
        repaint();//Piirrä menu
        
        
        thread = null;  // säikeen arvoksi asetetaan tyhjä
    }
    // MAIN
    public static void main(String args[]){
        new HyppyPeli().setVisible(true); 
        String filename = "COOLROBOT.mp3";      // tiedosto
        FileInputStream fis = null;           // virrat ja player olio
        BufferedInputStream bis = null;
        Player player = null;
        
        // kokeillaan avata virtoja ja playeri mp3-tiedostoon
        try {
            fis = new FileInputStream(filename);
            bis = new BufferedInputStream(fis);
            player = new Player(bis);
        }
        catch (Exception e) {
            System.out.println("Ongelmia MP3-tiedoston soittamisessa: " + filename);

            return;
        }       

        // soitetaan musaa omassa säikeessä ja tarkkaillaan toisessa
        if (player != null){
            // biisi säie
            BiisiThread mp3 = new BiisiThread("MP3 is playing", player);

            // position säie
            PosThread pos = new PosThread("Position checking", player);

            // käynnistetään säikeet (luokkien run()-metodit)
            mp3.start();
            pos.start();
        }    
    }
    // luokka toteuttaa JPaneelin (nimellä Piirtopaneeli), jonka avulla piirretään "peliä" näytölle
    class Piirtopaneeli extends JPanel{ 
   
        @Override
        public void paintComponent(Graphics g){ // piirtää komponentit seuraavan switch-casen mukaisesti "gameMode":n arvoksi seuraavia, 1 = menu, 2 = peli, 3 = pistetaulukko
            

            switch (gameMode) {
                 case 1: drawMenu(g); break; 
                 case 2: drawGame(g); break;
                 case 3: drawHighScore(g); break;
                 case 4: drawGameOver(g); break;
            }
            
        }
        
        // draw menu
        public void drawMenu(Graphics g) {
            setBackground(Color.WHITE);
            setLayout(null);
            g.drawImage(menu, 0, 0, this);
            //lisää napit
            remove(takaisinbtn);
            remove(nimiField);
            remove(tallennabtn);
            highscorebtn.setBounds(150, 450, 220, 30);
            lopetabtn.setBounds(150, 500, 220, 30);
            pelaabtn.setBounds(150, 400, 220, 30);
            add(pelaabtn);
            pelaabtn.setBackground(new Color(255, 133, 203));
            pelaabtn.setForeground(Color.BLACK);
            pelaabtn.setFocusPainted(false);
            pelaabtn.setFont(new Font("Tahoma", Font.BOLD, 12));
            add(highscorebtn);
            lopetabtn.setBackground(new Color(255, 133, 203));
            lopetabtn.setForeground(Color.BLACK);
            lopetabtn.setFocusPainted(false);
            lopetabtn.setFont(new Font("Tahoma", Font.BOLD, 12));
            add(lopetabtn);
            highscorebtn.setBackground(new Color(255, 133, 203));
            highscorebtn.setForeground(Color.BLACK);
            highscorebtn.setFocusPainted(false);
            highscorebtn.setFont(new Font("Tahoma", Font.BOLD, 12));
            // määrittää fontin ja sen koon
            Font fnt0 = new Font("arial", Font.BOLD, 50);
            g.setFont(fnt0);
            g.setColor(Color.BLACK);         
            //g.drawString("HYPPYPELI",120,200);  
            
        }
        // Draw GAme
        public void drawGame(Graphics g) {
               // poistaa aikaisemmin lisätyt napit (muuten jättää näkyviin pelinäkymmään kyseiset napit)
               
               remove(pelaabtn);
               remove(lopetabtn);
               remove(highscorebtn);
               remove(takaisinbtn);
               remove(nimiField);
               remove(tallennabtn);
               //piirrä pallo
               //g.setColor(ball.getColor());
               //g.drawImage(pelibg, liikuta, 0, this);  //taustakuva peliin
               
               
               
               g.setColor(ball.getColor());
               g.fillOval(ball.getX(), ball.getY(), ball.getSize(), ball.getSize());
               g.setColor(Color.black);
               g.drawOval(ball.getX(), ball.getY(), ball.getSize(), ball.getSize());
               //Piirrä palikat
               for(Block block : blocks){
               //g.setColor(Color.BLACK);
               
               if(ball.getX()== blocks.get(trip).getX()){
               block.color();
               g.setColor(block.getColor());
               }else g.setColor(block.getColor());
               g.fillRect (block.getX(), block.getY(), block.getSizeX(), block.getSizeY());
               g.setColor(Color.black);
               g.drawRect (block.getX(), block.getY(), block.getSizeX(), block.getSizeY());
               }
               // paulautteksi näyttöön seuraavat teksit ja arvot...
               g.setColor(Color.black); 
               Font fnt1 = new Font("arial", Font.PLAIN , 20);
               g.setFont(fnt1);
               g.drawString("Score :"+score, 20, 50);
            
        }
        public void drawGameOver(Graphics g){
                g.drawImage(gameover_img, 0, 0, this);
                Font fnt0 = new Font("arial", Font.BOLD, 60);
                g.setFont(fnt0);
                g.setColor(Color.BLACK);
                //g.drawString("Game Over",150,50);
                g.drawString(""+score, 245, 280);
                pelaabtn.setBackground(new Color(255, 133, 203));
                pelaabtn.setBounds(150, 450, 220, 30);
                add(pelaabtn);
                //takaisinbtn.setBounds(150, 500, 220, 30);
                //add(takaisinbtn);
                 nimiField.setBounds(150, 300, 220, 30);
                add(nimiField);
                tallennabtn.setBounds(150, 350, 220, 30);
               
                add(tallennabtn);
                tallennabtn.setBackground(new Color(255, 133, 203));
                tallennabtn.setForeground(Color.BLACK);
                tallennabtn.setFocusPainted(false);
                tallennabtn.setFont(new Font("Tahoma", Font.BOLD, 12));
        }
        
        // draw highscore
        public void drawHighScore(Graphics g) {
            g.drawImage(highscore_img, 0, 0, this);
            remove(pelaabtn);
            remove(lopetabtn);
            remove(highscorebtn);
            takaisinbtn.setBounds(150, 500, 220, 30);
            takaisinbtn.setBackground(new Color(255, 133, 203));
            takaisinbtn.setForeground(Color.BLACK);
            takaisinbtn.setFocusPainted(false);
            takaisinbtn.setFont(new Font("Tahoma", Font.BOLD, 12));
            add(takaisinbtn);      
            Font fnt0 = new Font("arial", Font.BOLD, 30);
            g.setFont(fnt0);
            g.setColor(Color.BLACK);         
            //g.drawString("HIGHSCORE",160,80);
            Font fnt1 = new Font("arial", Font.PLAIN , 15);
            g.setFont(fnt1);
            int x = 200;
            highscore.jarjesta();
            for(int i = 0;i<10;i++){
                Pelaaja pelaaja = highscore.palauta().get(i);
                g.drawString((i+1)+".", 155, x);
                g.drawString(pelaaja.getName(), 175, x);
                g.drawString(pelaaja.getScore()+"", 330, x);
                x+=25;
            }
        }
    }
}

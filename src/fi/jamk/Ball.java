/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.jamk;

import java.awt.Color;
import java.util.Random;

/**
 *
 * @author lasse
 */
public class Ball {
    private int x,y;
    private double dy;  // ns muutoskoordinaatti
    private int size;
    private  Color color;
    private int maxX,maxY;
    private int apu;
    
    public Ball(int maxX, int maxY){
        this.maxX = maxX; // tässä tapauksessa maxX kuvastaa ikkunan leveyttä
        this.maxY = maxY; // tässä tapauksessa maxY kuvastaa ikkunan korkeutta
        Random rand = new Random(); // asettaa uuden Random-tyyppisen muttujan nimellä "rand"
        this.x = (maxX / 2); // asettaa pallon keskelle leveyssuunnassa
        this.y = (maxY / 2); // asettaa pallon keskelle korkeussuunnassa
        this.dy = 2;  // muutoskoordinaatin arvoksi asetetaan 2
        this.size = 30; // tässä tapauksessa 30 on d eli diameter eli halkaisija
        this.color = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));  // arpoo uuden satunnaisen pallon värin RGB-arvot
    }
    //Painovoimasimulaatoori xD
    public void move(){
            
            this.y += this.dy; // (lisätään korkeuskoordinaattiin muutoskoordinaatti) = koordinaatin muutos
            this.dy += 0.5; // muutoskoordinaatin (se lisättävä muutosarvo) lisätään arvoon lisää 0.5 ts. "kiihtyvyys"
    }
    //Hiirenpainalluksella pallon suunta muuttuuu
    public void mmove(){
        
        this.dy = -6; // koska koodrinaattien laskeminen aloitetaan vasemmasta yläkulmasta 
                      // niin kun pallo putoaa niin y-arvo kasvaa, eli tässä tapauksessa korkeus nousee joten y-arvo pienenee
    }
    public void color(){
        Random rand = new Random();
        
        this.color = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
    }

    public int getX() { // palauttaa pallon x-koordinaatin
        return x;
    }


    public int getY() { // palauttaa pallon y-koordinaatin
        return y;
    }

    public int getSize() { // palauttaa pallon mitat
        return size;
    }

    public Color getColor() { // palauttaa pallon RGB-arvot eli värin
        return color;
    }
}

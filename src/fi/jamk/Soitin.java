/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package fi.jamk;

import javazoom.jl.player.Player;
import java.io.BufferedInputStream;
import java.io.FileInputStream;

/**
 *
 * @author H4273
 */
public class Soitin {
        // main()-metodi
    public void Soitin(){
        String filename = "COOLROBOT.mp3";      // tiedosto
        FileInputStream fis = null;           // virrat ja player olio
        BufferedInputStream bis = null;
        Player player = null;
        
        // kokeillaan avata virtoja ja playeri mp3-tiedostoon
        try {
            fis = new FileInputStream(filename);
            bis = new BufferedInputStream(fis);
            player = new Player(bis);
        }
        catch (Exception e) {
            System.out.println("Ongelmia MP3-tiedoston soittamisessa: " + filename);

            return;
        }       

        // soitetaan musaa omassa säikeessä ja tarkkaillaan toisessa
        if (player != null){
            // biisi säie
            BiisiThread mp3 = new BiisiThread("MP3 is playing", player);

            // position säie
            PosThread pos = new PosThread("Position checking", player);

            // käynnistetään säikeet (luokkien run()-metodit)
            mp3.start();
            pos.start();
        }    
    }

}


// luokka suorittaa MP3-tiedoston soittamista omassa säikeessä
class BiisiThread extends Thread {
    private Player player;


    // säikeen alustus
    public BiisiThread(String str, Player p) {
        super(str);
        this.player = p;
    }   


    // säikeen suorittaminen
    public void run(){
        try {
            player.play();
        }
        catch(Exception e) {
            System.out.println("MP3-soitossa ongelmia!");   
        }
    }
 }



// luokka tarkkailee MP3-tiedoston soittokohtaa
class PosThread extends Thread {
    private Player player;


    // säikeen alustus
    public PosThread(String str, Player p) {
        super(str);
        this.player = p;
    }
   
    // säikeen suorittaminen
    @Override
    public void run(){
        System.out.println("Soitto aloitettu!");
        while ( !player.isComplete() ) {
            int pos = player.getPosition();
           
            System.out.println("kohta (sek):" + pos / 1000 );
             
            try {   
              sleep(500);
            } catch (InterruptedException e) {
                System.out.println("Säie keskeytetty");
            }
        }
        System.out.println("Soitto lopetettu!");
    }
}


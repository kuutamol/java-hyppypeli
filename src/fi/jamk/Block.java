/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fi.jamk;

import java.awt.Color;
import java.util.Random;

/**
 *
 * @author lasse
 */
public class Block {
    private int dx;
    private int x;
    private int y;
    private int sizeX, sizeY;
    private Color color;
    private int kerroin; // määritellään muttuja "kerroin" 
    

    //Konstruktori
    public Block(int kerroin,int rand[]){
        this.kerroin = kerroin; // 
        int lisaa = 0;                                                  // luodaaan uusi myöhemmin hyädynnettävä apumuuttuja jota käytetään lisäämään blockien x-koordinaattiarvoa
        System.out.println("kerroin"+kerroin);                          // tulostaa Outputiin ns. palautteena sanan "kerroin" ja varsinaisen kertoimen
        if(kerroin%2 == 0){                                             // tarkastetaan parillisuus eli aloitetaan if-lause jossa ehtona on että jos ("kerroin" jaettuna kahdella)-lauseen jakojäännös on yhtä suuri kuin nolla niin toteutetaan seuraavaa...
            // luodaaan portin alempi palikka
            this.y = 400-rand[kerroin-2];                               // määritellään alemman blokin/seinän y-koordinaatin arvoksi 400 miinus satunnaisesti luodun luvun (eli rand-nimisen taulukon indeksiarvo miinus 2) 
            this.sizeY = 200+rand[kerroin-2];                           // määritellään blokin kooksi 200 plus satunnaisesti luotu luotu luku (eli saman taulukon indekiarvo miinus 2)
                    System.out.println("random"+rand[kerroin-2]);       // tulostetaan tarkastuksen vuoksi palautteeksi sana "random" ja kyseisen taulukon indeksiarvo miinus 2
                    System.out.println("SizeY"+this.sizeY);             // tulostetaan tarkastuksen vuoksi palautteeksi sana "SizeY" ja kyseisen blokin korkeus sizeY eli rectanglen korkeussuuntainen arvo
        }else{  // else eli jos ehto ei täyty eli ns "muussa tapauksessa"-tapaus
            this.y = -1;                                                 // ns "muussa tapauksessa"-tapauksessa palkin yläreuna on aina ruudun yläreunassa
            this.sizeY = 310-rand[kerroin-1];                           // määritellään alemman blokin/seinän y-koordinaatin arvoksi 300 miinus satunnaisesti luodun luvun (eli rand-nimisen taulukon indeksiarvo miinus 1)
                    System.out.println("random"+rand[kerroin-1]);       // tulostetaan tarkastuksen vuoksi palautteeksi sana "random" ja (eli rand-nimisen taulukon indeksiarvo miinus 1) 
                    System.out.println("SizeY"+this.sizeY);             // tulostetaan tarkastuksen vuoksi palautteeksi sana "SizeY" ja kyseisen blokin korkeus sizeY eli rectanglen korkeussuuntainen arvo
        }
        if(kerroin > 2){                                                // aloitetaan if-lause joka sisältää ehdon että jos "kerroin"-muuttujan arvo on isompi kuin 2
            if(kerroin%2 != 0){                                         // aloitetaan uusi aikaisemman if-lauseen sisäinen if-lause jossa ehtona se että jos "kerroin"-muuttujan jakojäännös on erisuuri kuin 0 niin suoritetaan seuraavaa...
                lisaa = (kerroin-1)*100;                                // ... "lisaa"-muuttujan arvoksi asetetaan (kerroin miinus 1) kerrottuna sadalla
            }else lisaa = (kerroin -2)*100;                             // muussa tapauksessa: "lisaa"-muuttujan arvoksi asetetaan (kerroin miinus kaksi) kerrotuuna sadalla
        }
        this.x = 500 +lisaa;                                            // ensimmäinen ns "aukko" on x:n kohdassa 500 plus "lisaa"-muuttujan arvo mikä on ensimmäisen aukon kohdalla nolla ja seuraavan 100 jne.

        //this.sizeY = 250; //palikan korkeus
        this.sizeX = 40;                                                //palikoiden leveysarvo aina vakiona 45
        this.dx = 3;                                                    //liikkumisnopeuden arvo aina vakiona 3
        Random rand2 = new Random();
        
        this.color = new Color(rand2.nextInt(255),rand2.nextInt(255),rand2.nextInt(255));
        System.out.println("x"+this.x);                                 // tulostetaan palautteeksi palikoiden eli blockien koordinaatit (ottaa blockien vasemmasta yläkulmasta)
        System.out.println("y"+this.y);

    }
    public void move(){                                                 // uusi metodi "move"
        this.x -= this.dx;                                              // muutetaan blockin koordinaattia "dx"-arvolla eli liikkuu vasemmalle 3 pikseliä kutsuttaessa
    }

    public int getDx() {                                                // 
        return dx;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public Color getColor() {
        return color;
    }
    public void color(){
        Random rand = new Random();
        
        this.color = new Color(rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
    }

}
